import React from "react"
import { Link } from "gatsby"

import styled from "styled-components"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const Bio = styled.div`
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  background-color: #fff;
  padding: 24px;
  margin-bottom: 20px;
  overflow-wrap: break-word;
  word-wrap: break-word;
  word-break: break-word;

  .author-photo {
    object-fit: cover;
    width: 80px;
    height: 80px;
    border-radius: 40px;
  }

  .author-name-container {
    margin-left: 15px;
  }
  .author-intro.jsx-2269311951 {
    font-family: "Brown-light", Arial, Verdana, sans-serif;
    color: #aaa;
    -webkit-letter-spacing: 1px;
    -moz-letter-spacing: 1px;
    -ms-letter-spacing: 1px;
    letter-spacing: 1px;
    font-size: 12px;
  }

  .author-name.jsx-2269311951 {
    font-family: "Cooper", Arial, Verdana, sans-serif;
    color: #333;
    font-size: 22px;
    line-height: 28px;
    margin-top: 4px;
  }

  @media (min-width: 700px) .author-quote.jsx-2269311951 {
    font-size: 18px;
    line-height: 22px;
  }

  .author-quote.jsx-2269311951 {
    font-family: "Brown-light", Arial, Verdana, sans-serif;
    color: #555;
    font-size: 14px;
    line-height: 18px;
    margin-top: 6px;
  }
`

const Intro = styled.div`
  background-color: #fff;
  margin-bottom: 20px;
`

const IndexPage = () => (
  <Layout>
    <SEO title="Brithday celebration!" />

    <Bio class="jsx-2269311951 author-bio">
      <div class="jsx-2269311951 author-photo">
        <img
          src="https://d2fqe3vzpyzs1r.cloudfront.net/original/upload-h0ojCBENY7zpEKrK.jpeg"
          class="jsx-2269311951"
        />
        <div class="jsx-2269311951 author-name-container">
          <div class="jsx-2269311951 author-intro">WRITTEN BY NUDGER</div>
          <div class="jsx-2269311951 author-name">Daniel Yim</div>
          <div class="jsx-2269311951 author-quote">
            Brogrammer, okay boyfriend, pants-wearer, and best friend.. Say hi
            on Insta: @danyims{" "}
          </div>
        </div>
      </div>
    </Bio>
    <Intro>
      <div class="jsx-504190678 content">
        <div class="jsx-504190678 name">
          Lands End Hike and Life-Changing Breakfast Sandwiches{" "}
        </div>
        <div class="jsx-504190678 description">
          Do a 4 mile scenic hike through Lands End, followed by SF’s best
          casual breakfast at Devil’s Teeth’s new location in the Richmond.{" "}
        </div>
        <div class="jsx-504190678 images">
          <img
            src="https://d2fqe3vzpyzs1r.cloudfront.net/original/user2_VNdJOAD7Z3LAJLzL-small.jpeg"
            alt="Nudger"
            class="jsx-504190678"
          />
          <img
            src="https://d2fqe3vzpyzs1r.cloudfront.net/original/user2_YX3UaJfHd8O6y61D-small.jpeg"
            alt="Nudger"
            class="jsx-504190678"
          />
          <img
            src="https://d2fqe3vzpyzs1r.cloudfront.net/original/user2_T3Fbd7V8pEQpEqUk-small.jpeg"
            alt="Nudger"
            class="jsx-504190678"
          />
          <img
            src="https://d2fqe3vzpyzs1r.cloudfront.net/original/user2_f6HaLcUhiyVM8rux-small.jpeg"
            alt="Nudger"
            class="jsx-504190678"
          />
          <img
            src="https://d2fqe3vzpyzs1r.cloudfront.net/original/user2_BfUmHHDcsGsP7HOP-small.jpeg"
            alt="Nudger"
            class="jsx-504190678"
          />
        </div>
        <div class="jsx-504190678 save-count">
          Saved by 2489 Nudgers in{" "}
          <a class="jsx-504190678" href="/the-nudge-app">
            our app
          </a>
          .
        </div>
        <div class="jsx-504190678 cover-photo">
          <img
            src="https://d2fqe3vzpyzs1r.cloudfront.net/original/upload-mAfqn06pzdPZJvXg.jpeg"
            alt="Cover photo"
            class="jsx-504190678"
          />
        </div>
      </div>
    </Intro>
    <p>Now go build something great.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/page-2/">Go to page 2</Link>
  </Layout>
)

export default IndexPage
